# Stratospheric impact on subseasonal forecast uncertainty in the Northern extratropics


This is the repository associated with the research article "Stratospheric impact on subseasonal forecast uncertainty in the Northern extratropics" by [Jonas Spaeth](https://orcid.org/0009-0006-4514-3787), [Philip Rupp](https://orcid.org/0000-0001-7833-1748), [Hella Garny](https://orcid.org/0000-0003-4960-2304) and [Thomas Birner](https://orcid.org/0000-0002-2966-3428) (links to ORCIDs):

*full citation and link to be added*

## How this repository is structured

- **1. retrieve data**
    - ECMWF S2S ensembles
    - teleconnection indices
        - u 60 10 hpa
        - QBO
        - ENSO
        - MJO
- **2. pre-process data**
    - re-structure S2S forecast dataset
    - compute diagnostics
        - Eddy activity
        - ensemble variance
        - latitude of maximum meridional pv gradient
        - ensemble variance
    - deseasonalize
        - mean quantities
        - variance quantities
- **3. analyze data**
    - group by polar vortex
    - group by tropical teleconnections
    - prepare for plots
        - fig 1: composites by polar vortex
        - fig 2: winds and eddy activity
        - fig 3: comparison of teleconnections
- **4. plotting**
    - fig 1: Z1000 mean and variance anomalies by polar vortex
    - fig 2: Upper-tropospheric circulation and Eddy activity by polar vortex
    - fig 3: Z1000 mean and variance anomalies by polar vortex and by tropical teleconnections

## data availability

- all data are freely available for download
    - era5: https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form
    - s2s: https://apps.ecmwf.int/datasets/data/s2s/levtype=sfc/type=cf/
    - teleconnection indices see notebook 1
- fully processed data used for figures in the manuscript are available at [data/processed/for-plotting](data/processed/for-plotting) 

## Notebooks

Notebooks 1-3 demonstrate how results can be reproduced using a small subset of the data.
Notebook 4 provides code used for plotting final figures.

## Correspondance

Jonas Spaeth: jonas.spaeth (at) physik.uni-muenchen.de